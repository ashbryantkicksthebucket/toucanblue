jQuery.noConflict();
jQuery( document ).ready(function($) {
    // console.log( 'ready!' );

    $( '#secondary' ).prepend( '<a href="#" class="btn btn-primary btn-block show-hide">Show Filters</a>' );

    $( '#secondary .widget' ).hide();

    $( '#secondary .show-hide' ).on( 'click', function(){
      var buttonText = $( '#secondary .show-hide' ).html();

      if ( buttonText == 'Show Filters' ) {
        $( this ).html( 'Hide Filters' ).toggleClass('active');
        $( '#secondary .widget' ).show('slow');

      }

      if ( buttonText == 'Hide Filters' ) {
        $( this ).html( 'Show Filters' ).toggleClass('active');
        $( '#secondary .widget' ).hide('slow');
      }

    });

});
