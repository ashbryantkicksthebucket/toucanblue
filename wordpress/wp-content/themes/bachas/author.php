<?php
/**
 * The template for displaying Author Archive pages
 *
 * Used to display archive-type pages for posts by an author.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage bachas_Themes
 * @since Huge Shop 1.0
 */

$bachas_opt = get_option( 'bachas_opt' );

get_header();

$bachas_postcount = 0;

if(isset($bachas_opt)){
	$bloglayout = 'nosidebar';
} else {
	$bloglayout = 'sidebar';
}
if(isset($bachas_opt['blog_layout']) && $bachas_opt['blog_layout']!=''){
	$bloglayout = $bachas_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bloglayout = $_GET['layout'];
}
$blogsidebar = 'right';
if(isset($bachas_opt['sidebarblog_pos']) && $bachas_opt['sidebarblog_pos']!=''){
	$blogsidebar = $bachas_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$blogsidebar = $_GET['sidebar'];
}
switch($bloglayout) {
	case 'sidebar':
		$blogclass = 'blog-sidebar';
		$blogcolclass = 9;
		Bachas::bachas_post_thumbnail_size('bachas-category-thumb');
		break;
	case 'largeimage':
		$blogclass = 'blog-large';
		$blogcolclass = 9;
		$bachas_postthumb = '';
		break;
	default:
		$blogclass = 'blog-nosidebar';
		$blogcolclass = 12;
		$blogsidebar = 'none';
		Bachas::bachas_post_thumbnail_size('bachas-post-thumb');
}
?>
<div class="main-container">
	<div class="title-breadcrumb">
		<div class="container">
			<div class="title-breadcrumb-inner">
				<header class="entry-header">
					<h1 class="entry-title"><?php if(isset($bachas_opt)) { echo esc_html($bachas_opt['blog_header_text']); } else { esc_html_e('Blog', 'bachas');}  ?></h1>
				</header>
				<?php Bachas::bachas_breadcrumb(); ?>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php if($blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			<div class="col-xs-12 <?php echo 'col-md-'.$blogcolclass; ?>">
				<div class="page-content blog-page <?php echo esc_attr($blogclass); if($blogsidebar=='left') {echo ' left-sidebar'; } if($blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php if ( have_posts() ) : ?>

						<?php
							/* Queue the first post, that way we know
							 * what author we're dealing with (if that is the case).
							 *
							 * We reset this later so we can run the loop
							 * properly with a call to rewind_posts().
							 */
							the_post();
						?>

						<header class="archive-header">
							<h1 class="archive-title"><?php printf( esc_html__( 'Author Archives: %s', 'bachas' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' ); ?></h1>
						</header><!-- .archive-header -->

						<?php
							/* Since we called the_post() above, we need to
							 * rewind the loop back to the beginning that way
							 * we can run the loop properly, in full.
							 */
							rewind_posts();
						?>

						<?php
						// If a user has filled out their description, show a bio on their entries.
						if ( get_the_author_meta( 'description' ) ) : ?>
						<div class="author-info archives">
							<div class="author-avatar">
								<?php
								/**
								 * Filter the author bio avatar size.
								 *
								 * @since Huge Shop 1.0
								 *
								 * @param int $size The height and width of the avatar in pixels.
								 */
								$author_bio_avatar_size = apply_filters( 'roadthemes_author_bio_avatar_size', 68 );
								echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
								?>
							</div><!-- .author-avatar -->
							<div class="author-description">
								<h2><?php printf( esc_html__( 'About %s', 'bachas' ), get_the_author() ); ?></h2>
								<p><?php the_author_meta( 'description' ); ?></p>
							</div><!-- .author-description	-->
						</div><!-- .author-info -->
						<?php endif; ?>

						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', get_post_format() ); ?>
						<?php endwhile; ?>
						
						<div class="pagination">
							<?php Bachas::bachas_pagination(); ?>
						</div>

					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php if( $blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
		
	</div>

	<?php if ( class_exists( 'WYSIJA_NL_Widget' ) ||  function_exists('bachas_brands_shortcode') ) { ?>
	<div class="brand-newsletter">
		<div class="container">
			<?php if(function_exists('bachas_brands_shortcode')) { ?>
			<div class="brands-logo other-page">
				<?php echo do_shortcode('[ourbrands]'); ?>
			</div>
			<?php } ?>
			<?php
			if ( isset($bachas_opt['newsletter_form']) ) { ?>
				<div class="newsletter other-page">
					<?php if(isset($bachas_opt['newsletter_title']) && $bachas_opt['newsletter_title']!='' ) {?>
					 <h3 class="newsletter-title"><?php echo esc_html($bachas_opt['newsletter_title']);?></h3>
					<?php } ?>

					<?php if(isset($bachas_opt['about_us_title']) && $bachas_opt['newsletter_text']!='' ) {?>
					 <p class="newsletter-text"><?php echo esc_html($bachas_opt['newsletter_text']);?></p>
					<?php } ?>

					<?php if(class_exists( 'WYSIJA_NL_Widget' )){
					the_widget('WYSIJA_NL_Widget', array(
					'form' => (int)$bachas_opt['newsletter_form'],
					'id_form' => 'newsletter1',
					'success' => '',
					)); ?>
				</div>
			<?php }
			} ?>
		</div>
	</div>
	<?php } ?>
</div>
<?php get_footer(); ?>