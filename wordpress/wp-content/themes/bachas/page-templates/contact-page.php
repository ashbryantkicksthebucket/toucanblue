<?php
/**
 * Template Name: Contact Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Huge Shop consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage bachas_Themes
 * @since Huge Shop 1.0
 */

$bachas_opt = get_option( 'bachas_opt' );

get_header();
?>
<div class="main-container contact-page">
	<div class="title-breadcrumb">
		<div class="container">
			<div class="title-breadcrumb-inner">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
				<?php Bachas::bachas_breadcrumb(); ?>
			</div>
		</div>
	</div>
	<div class="page-content">
		<div class="container">	
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
					<div class="entry-content">
						<?php the_content(); ?>
					</div><!-- .entry-content -->
				
				</article><!-- #post -->
			<?php endwhile; // end of the loop. ?>
		</div>
		<div class="brand-newsletter">
			<div class="container">
				<div class="brands-logo other-page">
					<?php echo do_shortcode('[ourbrands]'); ?>
				</div>
				<?php
				if ( isset($bachas_opt['newsletter_form']) ) { ?>
					<div class="newsletter other-page">
						<?php if(isset($bachas_opt['newsletter_title']) && $bachas_opt['newsletter_title']!='' ) {?>
						 <h3 class="newsletter-title"><?php echo esc_html($bachas_opt['newsletter_title']);?></h3>
						<?php } ?>

						<?php if(isset($bachas_opt['about_us_title']) && $bachas_opt['newsletter_text']!='' ) {?>
						 <p class="newsletter-text"><?php echo esc_html($bachas_opt['newsletter_text']);?></p>
						<?php } ?>

						<?php if(class_exists( 'WYSIJA_NL_Widget' )){
						the_widget('WYSIJA_NL_Widget', array(
						'form' => (int)$bachas_opt['newsletter_form'],
						'id_form' => 'newsletter1',
						'success' => '',
						)); ?>
					</div>
				<?php }
				} ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer('contact'); ?>