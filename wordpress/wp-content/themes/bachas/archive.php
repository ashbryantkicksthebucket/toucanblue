<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Huge Shop already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage bachas_Themes
 * @since Huge Shop 1.0
 */
$bachas_opt = get_option( 'bachas_opt' );

get_header();

$bachas_postcount = 0;

if(isset($bachas_opt)){
	$bloglayout = 'nosidebar';
} else {
	$bloglayout = 'sidebar';
}
if(isset($bachas_opt['blog_layout']) && $bachas_opt['blog_layout']!=''){
	$bloglayout = $bachas_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bloglayout = $_GET['layout'];
}
$blogsidebar = 'right';
if(isset($bachas_opt['sidebarblog_pos']) && $bachas_opt['sidebarblog_pos']!=''){
	$blogsidebar = $bachas_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$blogsidebar = $_GET['sidebar'];
}
switch($bloglayout) {
	case 'sidebar':
		$blogclass = 'blog-sidebar';
		$blogcolclass = 9;
		Bachas::bachas_post_thumbnail_size('bachas-category-thumb');
		break;
	case 'largeimage':
		$blogclass = 'blog-large';
		$blogcolclass = 9;
		$bachas_postthumb = '';
		break;
	default:
		$blogclass = 'blog-nosidebar';
		$blogcolclass = 12;
		$blogsidebar = 'none';
		Bachas::bachas_post_thumbnail_size('bachas-post-thumb');
}
?>
<div class="main-container">
	<div class="title-breadcrumb">
		<div class="container">
			<div class="title-breadcrumb-inner">
				<header class="entry-header">
					<h1 class="entry-title"><?php if(isset($bachas_opt)) { echo esc_html($bachas_opt['blog_header_text']); } else { esc_html_e('Blog', 'bachas');}  ?></h1>
				</header>
				<?php Bachas::bachas_breadcrumb(); ?>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			
			<?php if($blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			
			<div class="col-xs-12 <?php echo 'col-md-'.$blogcolclass; ?>">
				<div class="page-content blog-page <?php echo esc_attr($blogclass); if($blogsidebar=='left') {echo ' left-sidebar'; } if($blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php if ( have_posts() ) : ?>
						<header class="archive-header">
							<h1 class="archive-title"><?php
								if ( is_day() ) :
									printf( esc_html__( 'Daily Archives: %s', 'bachas' ), '<span>' . get_the_date() . '</span>' );
								elseif ( is_month() ) :
									printf( esc_html__( 'Monthly Archives: %s', 'bachas' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'bachas' ) ) . '</span>' );
								elseif ( is_year() ) :
									printf( esc_html__( 'Yearly Archives: %s', 'bachas' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'bachas' ) ) . '</span>' );
								else :
									_e( 'Archives', 'bachas' );
								endif;
							?></h1>
						</header><!-- .archive-header -->

						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/* Include the post format-specific template for the content. If you want to
							 * this in a child theme then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							get_template_part( 'content', get_post_format() );

						endwhile;
						?>
						
						<div class="pagination">
							<?php Bachas::bachas_pagination(); ?>
						</div>
						
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php if( $blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
	</div>
	<?php if ( class_exists( 'WYSIJA_NL_Widget' ) ||  function_exists('bachas_brands_shortcode') ) { ?>
	<div class="brand-newsletter">
		<div class="container">
			<?php if(function_exists('bachas_brands_shortcode')) { ?>
			<div class="brands-logo other-page">
				<?php echo do_shortcode('[ourbrands]'); ?>
			</div>
			<?php } ?>
			<?php
			if ( isset($bachas_opt['newsletter_form']) ) { ?>
				<div class="newsletter other-page">
					<?php if(isset($bachas_opt['newsletter_title']) && $bachas_opt['newsletter_title']!='' ) {?>
					 <h3 class="newsletter-title"><?php echo esc_html($bachas_opt['newsletter_title']);?></h3>
					<?php } ?>

					<?php if(isset($bachas_opt['about_us_title']) && $bachas_opt['newsletter_text']!='' ) {?>
					 <p class="newsletter-text"><?php echo esc_html($bachas_opt['newsletter_text']);?></p>
					<?php } ?>

					<?php if(class_exists( 'WYSIJA_NL_Widget' )){
					the_widget('WYSIJA_NL_Widget', array(
					'form' => (int)$bachas_opt['newsletter_form'],
					'id_form' => 'newsletter1',
					'success' => '',
					)); ?>
				</div>
			<?php }
			} ?>
		</div>
	</div>
	<?php } ?>
</div>
<?php get_footer(); ?>