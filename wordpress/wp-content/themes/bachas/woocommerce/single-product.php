<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' );
$bachas_opt = get_option( 'bachas_opt' );
?>
<div class="main-container">

	<div class="page-content">
	
		<div class="product-page">
			<div class="title-breadcrumb">
				<div class="container">
					<div class="title-breadcrumb-inner">
						<div class="entry-header">
							<h1><?php the_title(); ?></h1>
						</div>
						<?php do_action( 'woocommerce_before_main_content' ); ?>
					</div>
				</div>
			</div>
			<div class="product-view">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'single-product' ); ?>

				<?php endwhile; // end of the loop. ?>

				<?php
					/**
					 * woocommerce_after_main_content hook
					 *
					 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
					 */
					do_action( 'woocommerce_after_main_content' );
				?>

				<?php
					/**
					 * woocommerce_sidebar hook
					 *
					 * @hooked woocommerce_get_sidebar - 10
					 */
					//do_action( 'woocommerce_sidebar' );
				?>
			</div>
			
		</div>
		
		<div class="brand-newsletter">
			<div class="container">
				<div class="brands-logo other-page">
					<?php echo do_shortcode('[ourbrands]'); ?>
				</div>
				<?php
				if ( isset($bachas_opt['newsletter_form']) ) { ?>
					<div class="newsletter other-page">
						<?php if(isset($bachas_opt['newsletter_title']) && $bachas_opt['newsletter_title']!='' ) {?>
						 <h3 class="newsletter-title"><?php echo esc_html($bachas_opt['newsletter_title']);?></h3>
						<?php } ?>

						<?php if(isset($bachas_opt['about_us_title']) && $bachas_opt['newsletter_text']!='' ) {?>
						 <p class="newsletter-text"><?php echo esc_html($bachas_opt['newsletter_text']);?></p>
						<?php } ?>

						<?php if(class_exists( 'WYSIJA_NL_Widget' )){
						the_widget('WYSIJA_NL_Widget', array(
						'form' => (int)$bachas_opt['newsletter_form'],
						'id_form' => 'newsletter1',
						'success' => '',
						)); ?>
					</div>
				<?php }
				} ?>
			</div>
		</div>

	</div>
</div>
<?php get_footer( 'shop' ); ?>