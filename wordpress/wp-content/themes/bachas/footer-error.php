<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage bachas_Themes
 * @since Huge Shop 1.0
 */
$bachas_opt = get_option( 'bachas_opt' );
?>
		</div><!-- .page -->
	</div><!-- .wrapper -->
	<?php if ( isset($bachas_opt['back_to_top']) && $bachas_opt['back_to_top'] ) { ?>
	<div id="back-top" class="hidden-xs hidden-sm hidden-md"></div>
	<?php } ?>
	<?php wp_footer(); ?>
</body>
</html>