<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage bachas_Themes
 * @since Huge Shop 1.0
 */
 
$bachas_opt = get_option( 'bachas_opt' );
if(is_ssl()){
	$bachas_opt['logo_main']['url'] = str_replace('http:', 'https:', $bachas_opt['logo_main']['url']);
}
if ( ! class_exists( 'ReduxFrameworkPlugin' ) ) {
	$bachas_opt['sticky_header'] = false;
}
?>
	<div class="header-container">
		<div class="header">
			<div class="<?php if(isset($bachas_opt['sticky_header']) && $bachas_opt['sticky_header']) {echo 'header-sticky';} ?> <?php if ( is_admin_bar_showing() ) {echo 'with-admin-bar';} ?>">
				<div class="container header-inner">
					<div class="logo-wrap">
						<div class="global-table">
							<div class="global-row">
								<div class="global-cell">
									<?php if( isset($bachas_opt['logo_main']['url']) ){ ?>
										<div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo esc_url($bachas_opt['logo_main']['url']); ?>" alt="" /></a></div>
									<?php
									} else { ?>
										<h1 class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
										<?php
									} ?>
								</div>
							</div>
						</div>
					</div>

					<div class="menu-wrap visible-large">	
						<div class="global-table">
							<div class="global-row">
								<div class="global-cell">
									<div class="horizontal-menu">
										<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => 'primary-menu-container', 'menu_class' => 'nav-menu' ) ); ?>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="content-header">
						<div class="global-table">
							<div class="global-row">
								<div class="global-cell">
									<?php if(class_exists('WC_Widget_Product_Search') ) { ?>
										<div class="header-search">
											<div class="search-icon">
												<span class="pe-7s-search"></span>
											</div>
											<?php the_widget('WC_Widget_Product_Search', array('title' => 'Search')); ?>
										</div>
									<?php } ?>
									<!-- end header-search -->

									<?php if ( class_exists( 'WC_Widget_Cart' ) ) {
										the_widget('Custom_WC_Widget_Cart'); 
									} ?>
									<!-- end header-cart -->

									<div class="vmenu-toggler">
										<?php if ( has_nav_menu( 'topmenu' ) ) { ?>
										<div class="vmenu-toggler-button">
											<span class="pe-7s-config"></span>
										</div>
										<?php } ?>
										<div class="vmenu-content">
											<?php if ( has_nav_menu( 'topmenu' ) ) { ?>
											<div class="title-vmenu">
												<?php esc_html_e('My account', 'bachas') ?>
											</div>
											<?php wp_nav_menu( array( 'theme_location' => 'topmenu', 'container_class' => 'top-menu-container', 'menu_class' => 'nav-menu' ) ); ?>
											<?php } ?>
											<?php if (class_exists('SitePress')) { ?>
											<div class="title-vmenu">
												<?php esc_html_e('Currency', 'bachas');?>
											</div>	
											<?php do_action('currency_switcher'); ?>
											<div class="title-vmenu">
												<?php esc_html_e('Language', 'bachas');?>
											</div>	
											<?php do_action('icl_language_selector'); ?>
											<?php } ?>
										</div>
									</div>
									<!-- end header-setting -->
								</div>
							</div>
						</div>
					</div>

					<div class="visible-small mobile-menu">
						<div class="nav-container">
							<div class="mbmenu-toggler">
								<?php 
								if(isset($bachas_opt['mobile_menu_label']) && $bachas_opt['mobile_menu_label']!=''){
									echo esc_html($bachas_opt['mobile_menu_label']);
								} else {
									echo esc_html('Menu', 'bachas');
								}
								?>
								<span class="mbmenu-icon"><i class="fa fa-bars"></i></span>
							</div>
							<?php wp_nav_menu( array( 'theme_location' => 'mobilemenu', 'container_class' => 'mobile-menu-container', 'menu_class' => 'nav-menu' ) ); ?>
						</div>
					</div>
					<!-- end megamenu mobile -->
				</div>
			</div>
		</div><!-- .header -->
		<div class="clearfix"></div>
	</div>