<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage bachas_Themes
 * @since Huge Shop 1.0
 */

$bachas_opt = get_option( 'bachas_opt' );

get_header();

?>
	<div class="main-container error404">
		<div class="container">
			<div class="search-form-wrapper">
				<h1><?php esc_html_e( "404", 'bachas' ); ?></h1>
				<h2><?php esc_html_e( "Opps! PAGE NOT BE FOUND", 'bachas' ); ?></h2>
				<p class="home-link"><?php esc_html_e( "Sorry but the page you are looking for does not exist, have been removed, name changed or is temporarity unavailable.", 'bachas' ); ?></p>
				<?php get_search_form(); ?>
				<a class="button" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_html_e( 'Back to home', 'bachas' ); ?>"><?php esc_html_e( 'Back to home page', 'bachas' ); ?></a>
			</div>
		</div>
		<div class="brand-newsletter">
			<div class="container">
				<div class="brands-logo other-page">
					<?php echo do_shortcode('[ourbrands]'); ?>
				</div>
				<?php
				if ( isset($bachas_opt['newsletter_form']) ) { ?>
					<div class="newsletter other-page">
						<?php if(isset($bachas_opt['newsletter_title']) && $bachas_opt['newsletter_title']!='' ) {?>
						 <h3 class="newsletter-title"><?php echo esc_html($bachas_opt['newsletter_title']);?></h3>
						<?php } ?>

						<?php if(isset($bachas_opt['about_us_title']) && $bachas_opt['newsletter_text']!='' ) {?>
						 <p class="newsletter-text"><?php echo esc_html($bachas_opt['newsletter_text']);?></p>
						<?php } ?>

						<?php if(class_exists( 'WYSIJA_NL_Widget' )){
						the_widget('WYSIJA_NL_Widget', array(
						'form' => (int)$bachas_opt['newsletter_form'],
						'id_form' => 'newsletter1',
						'success' => '',
						)); ?>
					</div>
				<?php }
				} ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>