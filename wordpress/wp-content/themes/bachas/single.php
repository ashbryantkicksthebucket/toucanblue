<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage bachas_Themes
 * @since Huge Shop 1.0
 */

$bachas_opt = get_option( 'bachas_opt' );

get_header();
?>
<?php 
$bloglayout = 'nosidebar';
if(isset($bachas_opt['blog_layout']) && $bachas_opt['blog_layout']!=''){
	$bloglayout = $bachas_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bloglayout = $_GET['layout'];
}
$blogsidebar = 'right';
if(isset($bachas_opt['sidebarblog_pos']) && $bachas_opt['sidebarblog_pos']!=''){
	$blogsidebar = $bachas_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$blogsidebar = $_GET['sidebar'];
}
switch($bloglayout) {
	case 'sidebar':
		$blogclass = 'blog-sidebar';
		$blogcolclass = 9;
		break;
	default:
		$blogclass = 'blog-nosidebar'; //for both fullwidth and no sidebar
		$blogcolclass = 12;
		$blogsidebar = 'none';
}
?>
<div class="main-container page-wrapper">
	<div class="title-breadcrumb">
		<div class="container">
			<div class="title-breadcrumb-inner">
				<header class="entry-header">
					<h1 class="entry-title"><?php if(isset($bachas_opt)) { echo esc_html($bachas_opt['blog_header_text']); } else { esc_html_e('Blog', 'bachas');}  ?></h1>
				</header>
				<?php Bachas::bachas_breadcrumb(); ?>
			</div>
		</div>
		
	</div>
	<div class="container">
		<div class="row">

			<?php if($blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			
			<div class="col-xs-12 <?php echo 'col-md-'.$blogcolclass; ?>">
				<div class="page-content blog-page single <?php echo esc_attr($blogclass); if($blogsidebar=='left') {echo ' left-sidebar'; } if($blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', get_post_format() ); ?>

						<?php comments_template( '', true ); ?>
						
						<!--<nav class="nav-single">
							<h3 class="assistive-text"><?php esc_html_e( 'Post navigation', 'bachas' ); ?></h3>
							<span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'bachas' ) . '</span> %title' ); ?></span>
							<span class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'bachas' ) . '</span>' ); ?></span>
						</nav><!-- .nav-single -->
						
					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
			<?php if( $blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
	</div>
	
	<?php if ( class_exists( 'WYSIJA_NL_Widget' ) ||  function_exists('bachas_brands_shortcode') ) { ?>
	<div class="brand-newsletter">
		<div class="container">
			<?php if(function_exists('bachas_brands_shortcode')) { ?>
			<div class="brands-logo other-page">
				<?php echo do_shortcode('[ourbrands]'); ?>
			</div>
			<?php } ?>
			<?php
			if ( isset($bachas_opt['newsletter_form']) ) { ?>
				<div class="newsletter other-page">
					<?php if(isset($bachas_opt['newsletter_title']) && $bachas_opt['newsletter_title']!='' ) {?>
					 <h3 class="newsletter-title"><?php echo esc_html($bachas_opt['newsletter_title']);?></h3>
					<?php } ?>

					<?php if(isset($bachas_opt['about_us_title']) && $bachas_opt['newsletter_text']!='' ) {?>
					 <p class="newsletter-text"><?php echo esc_html($bachas_opt['newsletter_text']);?></p>
					<?php } ?>

					<?php if(class_exists( 'WYSIJA_NL_Widget' )){
					the_widget('WYSIJA_NL_Widget', array(
					'form' => (int)$bachas_opt['newsletter_form'],
					'id_form' => 'newsletter1',
					'success' => '',
					)); ?>
				</div>
			<?php }
			} ?>
		</div>
	</div>
	<?php } ?>

</div>

<?php get_footer(); ?>