03/June/2016: Released version 1.0
	-	You need read instruction in documentation folder

	
18/June/2016: Updated to version 1.0.1
	- Updated the WooCommerce template files for 2.6.1 version
	- Updated css for new WooCommerce 2.6.1
	-------------------------------
	Updated details:
		- Updated "less/woocommerce.less"
		- Updated "woocommerce/archive-product.php"
		- Updated "woocommerce/content-product.php"
		- Updated "woocommerce/content-product-archive.php"
		- Updated "woocommerce/content-product_cat.php"
		- Updated "functions.php"
		
		- Removed "woocommerce/myaccount/form-login.php"
		- Removed "woocommerce/myaccount/form-lost-password.php"
		- Removed "woocommerce/myaccount/my-address.php"
		- Removed "woocommerce/myaccount/view-order.php"
	-------------------------------