<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage bachas_Themes
 * @since Huge Shop 1.0
 */
$bachas_opt = get_option( 'bachas_opt' );
?>
	<div class="footer">
		<?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
		<div class="footer-main">
			<div class="container">
				<div class="row">
					<?php if(isset($bachas_opt['about_us']) && $bachas_opt['about_us']!=''){ ?>
						<div class="footer-col col-sm-12 col-md-3">
							<div class="widget widget_about_us">

							<?php if(isset($bachas_opt['about_us_title']) && $bachas_opt['about_us_title']!='' ) {?>
							 <h3 class="widget-title"><?php echo esc_html($bachas_opt['about_us_title']);?></h3>
							<?php } ?>

							<?php echo wp_kses($bachas_opt['about_us'], array(
									'span' => array(
										'class' => array(),
									),
									'a' => array(
										'href' => array(),
										'title' => array()
									),
									'div' => array(
										'class' => array(),
									),
									'img' => array(
										'src' => array(),
										'alt' => array()
									),
									'h3' => array(
										'class' => array(),
									),
									'ul' => array(),
									'li' => array(),
									'i' => array(
										'class' => array()
									),
									'br' => array(),
									'em' => array(),
									'strong' => array(),
									'p' => array(),
							)); ?>
							
							</div>

							<?php
							if(isset($bachas_opt['social_icons'])) {
								echo '<ul class="social-icons">';
								foreach($bachas_opt['social_icons'] as $key=>$value ) {
									if($value!=''){
										if($key=='vimeo'){
											echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>';
										} else {
											echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';
										}
									}
								}
								echo '</ul>';
							}?>

						</div>
					<?php } ?>
					<!-- end about_us -->

					<?php
					if( isset($bachas_opt['footer_menu1']) && $bachas_opt['footer_menu1']!='' ) {
						$menu1_object = wp_get_nav_menu_object( $bachas_opt['footer_menu1'] );
						$menu1_args = array(
							'menu_class'      => 'nav_menu',
							'menu'         => $bachas_opt['footer_menu1'],
						); ?>
						<div class="footer-col col-sm-6  col-md-2">
							<div class="widget widget_menu">
								<h3 class="widget-title"><?php echo esc_html($menu1_object->name); ?></h3>
								<?php wp_nav_menu( $menu1_args ); ?>
							</div>
						</div>
					<?php }
					// end footer_menu1

					if( isset($bachas_opt['footer_menu2']) && $bachas_opt['footer_menu2']!='' ) {
						$menu2_object = wp_get_nav_menu_object( $bachas_opt['footer_menu2'] );
						$menu2_args = array(
							'menu_class'      => 'nav_menu',
							'menu'         => $bachas_opt['footer_menu2'],
						); ?>
						<div class="footer-col col-sm-6  col-md-2">
							<div class="widget widget_menu">
								<h3 class="widget-title"><?php echo esc_html($menu2_object->name); ?></h3>
								<?php wp_nav_menu( $menu2_args ); ?>
							</div>
						</div>
					<?php }
					// end footer_menu2

					if( isset($bachas_opt['footer_menu3']) && $bachas_opt['footer_menu3']!='' ) {
						$menu3_object = wp_get_nav_menu_object( $bachas_opt['footer_menu3'] );
						$menu3_args = array(
							'menu_class'      => 'nav_menu',
							'menu'         => $bachas_opt['footer_menu3'],
						); ?>
						<div class="footer-col col-sm-6  col-md-2">
							<div class="widget widget_menu">
								<h3 class="widget-title"><?php echo esc_html($menu3_object->name); ?></h3>
								<?php wp_nav_menu( $menu3_args ); ?>
							</div>
						</div>
					<?php } ?>
					<!-- end footer_menu3 -->

					<?php if(isset($bachas_opt['contact_us']) && $bachas_opt['contact_us']!=''){ ?>

						<div class="footer-col col-sm-6 col-md-3">

							<div class="widget widget_contact_us">

								<?php if(isset($bachas_opt['contact_us_title']) && $bachas_opt['contact_us_title']!='' ) {?>
								 <h3 class="widget-title"><?php echo esc_html($bachas_opt['contact_us_title']);?></h3>
								<?php } ?>

								<?php echo wp_kses($bachas_opt['contact_us'], array(
										'p' => array(
											'class' => array(),
										),
										'span' => array(
											'class' => array(),
										),
										'a' => array(
											'href' => array(),
											'title' => array()
										),
										'div' => array(
											'class' => array(),
										),
										'img' => array(
											'src' => array(),
											'alt' => array()
										),
										'h3' => array(
											'class' => array(),
										),
										'ul' => array(
											'class' => array(),
										),
										'li' => array(
											'class' => array(),
										),
										'i' => array(
											'class' => array(),
										),
										'br' => array(),
										'em' => array(),
										'strong' => array(),
									)); ?>
							</div>
							
						</div>
					<?php } ?>
					<!-- end contact_us -->

				</div>
			</div>
		</div>
		<?php } ?>

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="widget-copyright">
							<?php 
							if( isset($bachas_opt['copyright']) && $bachas_opt['copyright']!='' ) {
								echo wp_kses($bachas_opt['copyright'], array(
									'a' => array(
										'href' => array(),
										'title' => array()
									),
									'br' => array(),
									'em' => array(),
									'strong' => array(),
								));
							} else {
								echo 'Copyright <a href="'.esc_url( home_url( '/' ) ).'">'.get_bloginfo('name').'</a> '.date('Y').'. All Rights Reserved';
							}
							?>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="widget-payment">
							<?php if(isset($bachas_opt['payment_icons']) && $bachas_opt['payment_icons']!='' ) {
								echo wp_kses($bachas_opt['payment_icons'], array(
									'a' => array(
										'href' => array(),
										'title' => array()
									),
									'img' => array(
										'src' => array(),
										'alt' => array()
									),
								)); 
							} ?>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	