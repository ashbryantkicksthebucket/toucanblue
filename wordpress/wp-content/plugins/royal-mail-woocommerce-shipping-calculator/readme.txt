=== WooCommerce Royal Mail Shipping Calculator ===
Contributors: waseem_senjer, wprubyplugins
Donate link: https://wpruby.com
Tags: woocommerce,shipping, woocommerce extension, UK, Royal Mail,admin,shipping method
Requires at least: 3.5.1
Tested up to: 4.7.3
Stable tag: 1.2.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WooCommerce Royal Mail Shipping Calculator is a Wordpress Plugin that integrate the Royal Mail service, it will calculate the shipping costs for your customers. 
	

== Description ==
WooCommerce Royal Mail Shipping Calculator is a Wordpress Plugin that integrate the Royal Mail service, it will calculate the shipping cost and the delivery time for your customer. 
	
If you have any issues or questions, please open a thread in the WordPress.org forum or you can open a [support ticket](wpruby.com/submit-ticket/ "support ticket") on our website.

== Screenshots ==

1. Royal Mail Shipping Settings Page.
2. Checkout Page Preview.



== Installation ==



= Using The WordPress Dashboard =

1. Navigate to the 'Add New' in the plugins dashboard
2. Search for 'WooCommerce Royal Mail Shipping Calculator'
3. Click 'Install Now'
4. Activate the plugin on the Plugin dashboard

= Uploading in WordPress Dashboard =

1. Navigate to the 'Add New' in the plugins dashboard
2. Navigate to the 'Upload' area
3. Select `royalmail-woocommerce-shipping-calculator.zip` from your computer
4. Click 'Install Now'
5. Activate the plugin in the Plugin dashboard

= Using FTP =

1. Download `royalmail-woocommerce-shipping-calculator.zip`
2. Extract the `royalmail-woocommerce-shipping-calculator` directory to your computer
3. Upload the `royalmail-woocommerce-shipping-calculator` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin dashboard




== Changelog ==
= 1.2.3 10/04/2017 =
* ADDED: update the shipping prices for Royal Mail 2017 standards


= 1.2.2 23/03/2017 =
* FIXED: if Small Parcel is selected keep adding items to multiple small parcels

= 1.2.1 09/3/2017 =
* FIXED: Compatibility with Kite Print and Dropshipping on Demand plugin

= 1.2.0 21/6/2016 =
* ADDED: Compatibility with WooCommerce less than 2.6.0
* ADDED: Support of UK Guaranteed and UK Confirmed shipping options.

= 1.1.0 =
* ADDED: Compatibility with WooCommerce Shipping Zones which introduced in WC 2.6.0

= 1.0.1 =
UPDATED: The last prices from Royal Mail.

= 1.0.0 =
* Initial release.

== Upgrade Notice == 
* Initial Release